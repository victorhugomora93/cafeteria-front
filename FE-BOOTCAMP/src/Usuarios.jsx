import { useState, useEffect } from "react";

const Usuarios = () => {
  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8000/users/", {
      method: "GET" /* or POST/PUT/PATCH/DELETE */,
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem("accessToken")
        )}`,
        "Content-Type": "application/json",
      },
      /*  */
    })
      .then((res) => res.json())
      .then((data) => {
        setUsuarios(data);
        console.log(data);
      });
  }, []);

  return (
    <>
      <ul>
        {usuarios.map((usuario) => {
          return <li> {usuario.username} </li>;
        })}
      </ul>
    </> 
  );
};

export default Usuarios;
