import { useNavigate } from "react-router-dom"

export function PedidoCard({pedido}) {

const navigate = useNavigate()
  return (
    <div className="bg-zinc-300 p-3 hover:bg-zinc-800 hover:cursor-pointer"
    
      onClick={() => {
        navigate('/pedido/' + pedido.id)
      }}

    >
         
         <p className="text-slate-400"> {pedido.id} </p>
         <p><span className="font-bold uppercase"> Mesa {pedido.mesa} </span> </p>
         <span className="text-slate-400"> Total: {pedido.total} </span>
         <p> <span className="text-slate-400"> Estado: {pedido.estado} </span> </p>
         <span className="text-slate-400"> Fecha y Hora{pedido.fecha_hora} </span>
         <span className="text-slate-400"> Producto {pedido.lista_productos} </span>
    </div>
  )
}
