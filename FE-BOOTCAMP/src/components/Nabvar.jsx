import {Link} from 'react-router-dom';


export function Nabvar() {

  

  return (
    <div className='flex justify-between py-3'>
       <Link to="/home">
        <h1 className="font-bold text-3x1 mb-4">  Home </h1>
       </Link>
       <button className="bg-indigo-300 px-3 py-2 rounded-lg">
         <Link to="/product-create"> Crear Producto </Link>
       </button>
       <button className="bg-indigo-300 px-3 py-2 rounded-lg">
          <Link to="/product"> Listar Productos </Link>
       </button>
       <button className="bg-indigo-300 px-3 py-2 rounded-lg">
         <Link to="/pedido-create"> Crear Pedido </Link>
       </button>
       <button className="bg-indigo-300 px-3 py-2 rounded-lg">
          <Link to="/pedido"> Listar Pedidos </Link>
       </button>
    </div>
   
  )
}


