import { useEffect, useState } from "react"
import { getAllPedidos } from "../api/pedido.api"
import { PedidoCard } from "./PedidoCard";

 export function PedidoList() {

    const [pedido, setPedido] = useState([]);

    useEffect(()=> {
        async function loadPedido() {
            const res = await getAllPedidos();
            setPedido(res.data);
        }
        loadPedido();
    },[]);

  return (
            <div className="grid grid-cols-3 gap-3" >
             {pedido.map(pedido =>(
                   <PedidoCard key={pedido.id} pedido={pedido} />
               ))}
            </div>
         );

}


