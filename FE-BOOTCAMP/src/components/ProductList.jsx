import { useEffect, useState } from "react"
import { getAllProducts } from "../api/product.api"
import { ProducCard } from "./ProducCard";

 export function ProductList() {

    const [product, setProduct] = useState([]);

    useEffect(()=> {
        async function loadProduct() {
            const res = await getAllProducts();
            setProduct(res.data);
        }
        loadProduct();
    },[]);

  return (
            <div className="grid grid-cols-3 gap-3" >
             {product.map(product =>(
                   <ProducCard key={product.id} product={product} />
               ))}
            </div>
         );

}


