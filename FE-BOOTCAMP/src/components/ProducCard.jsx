import { useNavigate } from "react-router-dom"

export function ProducCard({product}) {

const navigate = useNavigate()
  return (
    <div className="bg-zinc-300 p-3 hover:bg-zinc-800 hover:cursor-pointer"
    
      onClick={() => {
        navigate('/product/' + product.id)
      }}

    >
         <h1 className="font-bold uppercase"> {product.nombre}</h1> 
         <p className="text-slate-400"> {product.precio} </p>
    </div>
  )
}

