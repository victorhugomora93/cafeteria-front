import { useState, useEffect } from "react";
import "./App.css";
import Login from "./Login";
import Home from "./Home";
import jwtDecode from "jwt-decode";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Pedidos from "./Pedidos";
import Productos from "./Productos";
import { Nabvar } from "./components/Nabvar";
import { ProductPage } from "./pages/ProductPage";
import { ProductFormPage } from "./pages/ProductFormPage";
import { PedidoPage } from "./pages/PedidoPage";
import { PedidoFormPage } from "./pages/PedidoFormPage";
import { Toaster } from "react-hot-toast";
import Usuarios from "./Usuarios";

function App() {

  const [userId, setUserId] = useState(null)

  useEffect(() => {
    const token = window.localStorage.getItem("accessToken");
    if (token) {
      setUserId(jwtDecode(JSON.parse(token)).user_id);
    }
  }, []);

  const onLoginHandler = (userId) => {
    console.log(userId);
    setUserId(userId);
  };

  const onLogoutHandler = () => {
    setUserId(null);
    window.localStorage.removeItem("accessToken");
  }; 

  return (
    <>
      <div className="container mx-auo">
        {/* {userId ? (
          <Home onLogout={onLogoutHandler} userId={userId} />
        ) : (
         <Login onLogin={onLoginHandler} />
        ) }*/}
        <BrowserRouter>
          <Nabvar />
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/home" element={ <Home userId={userId}/>}/>
            <Route path="/pedidos" element={<Pedidos />} />
            <Route path="/productos" element={<Productos />} />
            <Route path="/product" element={<ProductPage />} />
            <Route path="/product-create" element={<ProductFormPage />} />
            <Route path="/product/:id" element={<ProductFormPage />} />
            <Route path="/pedido" element={<PedidoPage />} />
            <Route path="/pedido-create" element={<PedidoFormPage />} />
            <Route path="/pedido/:id" element={<PedidoFormPage />} />
            <Route path="/usuario/" element={<Usuarios />} />
          </Routes>
          <Toaster />
        </BrowserRouter>
      </div>
    </>
  );
}

export default App;
