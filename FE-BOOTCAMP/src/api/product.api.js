import axios from 'axios'

const productApi = axios.create({
    baseURL: 'http://localhost:8000/productos/'
})

export const getAllProducts = () => {

    return productApi.get('/',{
      headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json'
    }}

    )} 

    export const getProduct = (id) => {
        return productApi.get(`/${id}/`,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

    export const createProduct = (product) => {
      return productApi.post('/',product,{
         headers: {
             Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
             'Content-Type': 'application/json'
     }}
    
    )}

    export const deleteProduct = (id) => {
        return productApi.delete(`/${id}`,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

      export const updateProduct = (id, product) => {
        return productApi.put(`/${id}/`, product,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

    
    