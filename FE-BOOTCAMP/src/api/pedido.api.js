import axios from 'axios'

const pedidoApi = axios.create({
    baseURL: 'http://localhost:8000/pedidos/'
})

export const getAllPedidos = () => {

    return pedidoApi.get('/',{
      headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json'
    }}

    )} 

    export const getPedido = (id) => {
        return pedidoApi.get(`/${id}/`,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

    export const createPedido = (pedido) => {
      return pedidoApi.post('/', pedido,{
         headers: {
             Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
             'Content-Type': 'application/json'
     }}
    
    )}

    export const deletePedido = (id) => {
        return pedidoApi.delete(`/${id}`,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

      export const updatePedido = (id, pedido) => {
        return pedidoApi.put(`/${id}/`, pedido,{
           headers: {
               Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
               'Content-Type': 'application/json'
       }}
      
      )}

    
    