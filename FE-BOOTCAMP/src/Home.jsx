import { useState, useEffect } from 'react'
import Productos from './Productos'
import Pedidos from './Pedidos'

const Home = ({ onLogout, userId }) => {
  const [user, setUser] = useState()

  useEffect(() => {
    fetch('http://localhost:8000/users/' + userId, {
      method: 'GET' /* or POST/PUT/PATCH/DELETE */,
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((userData) => {
        setUser(userData)
        console.log(userData)
      })
  }, [])

  const logoutHandler = () => {
    onLogout()
  }

  /* const role = user ? user.group_name : null
  const contentPr = role && role === 'recepcion' ? <Productos /> : <p>Home Page</p>
  const content = role && role === 'recepcion' ? <Pedidos /> : <p>Home Page</p> */

  return (
    <>
      <button onClick={logoutHandler}>Logout</button>
      {user && <>
        <h1>Bienvenido {user.username}!</h1>
        <p>{user.group_name}</p>
      </>}
    </>
  )
}

export default Home
