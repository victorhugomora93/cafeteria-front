import { useForm } from "react-hook-form";
import { useEffect } from "react";
import { createPedido, deletePedido, updatePedido, getPedido } from "../api/pedido.api";
import { useNavigate, useParams } from "react-router-dom";
import {toast} from 'react-hot-toast'

export function PedidoFormPage() {

  const {register, handleSubmit, formState:{errors}, setValue} = useForm();
  const navigate = useNavigate();
  const params = useParams();
  console.log(params)

  const onSubmit = handleSubmit(async (data) => {

    if(params.id){
      await updatePedido(params.id, data);
      toast.success('Producto Modificado',{
        position: "bottom-right"
     })
    }else{
       await createPedido(data);
       toast.success('Producto Creado',{
          position: "bottom-right"
       })
    }
    navigate("/pedido")
  });

  useEffect(() =>{
    async function loadPedido(){
      if(params.id){
        const res = await getPedido(params.id);
        console.log(res)
        setValue('id', res.data.id)
        setValue('mesa', res.data.mesa)
        setValue('total', res.data.total)
        setValue('estado', res.data.estado)
        setValue('fecha_hora', res.data.fecha_hora)
        setValue('lista_productos', res.data.lista_productos)
      }
    }
    loadPedido();
  },[])

  return (
    <div className="max-w-xl mx-auto" >
      <form onSubmit={onSubmit}>
        <input 
          type="text"
          placeholder="Id"
          {...register("id", { required: true })}
          className="bg-zinc-500 p-3 rounded-lg block w-full mb-3"
          />
          {errors.id && <span>El campo es requerido</span>}
        <input 
          type="text"
          placeholder="Numero de Mesa"
          {...register("mesa", { required: true })}
          />
          {errors.nombre && <span>El campo es requerido</span>}
        <input 
          type="text" 
          placeholder="Total"
          {...register("total", { required: true })}
          />
          {errors.precio && <span>El campo es requerido</span>}
          <input 
          type="text" 
          placeholder="Estado"
          {...register("estado", { required: true })}
          />
          {errors.precio && <span>El campo es requerido</span>}
          <input 
          type="text" 
          placeholder="Fecha y Hora"
          {...register("fecha_hora", { required: true })}
          />
          {errors.precio && <span>El campo es requerido</span>}
          <input 
          type="text" 
          placeholder="Producto"
          {...register("lista_productos", { required: true })}
          />
          {errors.precio && <span>El campo es requerido</span>}
        <button className="bg-indigo-400 p-3 rounded-lg block w-full mt-3"> 
          Guardar</button>

      </form>
        {params.id && (<button
        className="bg-red-500 p-3 rounded-lg w-48 mt-3"
        onClick={
         async() => {
           const accepted = window.confirm('seguro que quiere borrar?');
           if(accepted){
              await deletePedido(params.id);
              toast.success('Producto Eliminado',{
                position: "bottom-right"
             });
             navigate("/pedido")   
            }
          }}
          > 
          Borrar 
          </button>
          )}

      </div>  
  );
}
