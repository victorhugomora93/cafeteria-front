import { useForm } from "react-hook-form";
import { useEffect } from "react";
import { createProduct, deleteProduct, updateProduct, getProduct } from "../api/product.api";
import { useNavigate, useParams } from "react-router-dom";
import {toast} from 'react-hot-toast'

export function ProductFormPage() {

  const {register, handleSubmit, formState:{errors}, setValue} = useForm();
  const navigate = useNavigate();
  const params = useParams();
  console.log(params)

  const onSubmit = handleSubmit(async (data) => {

    if(params.id){
      await updateProduct(params.id, data);
      toast.success('Producto Modificado',{
        position: "bottom-right"
     })
    }else{
       await createProduct(data);
       toast.success('Producto Creado',{
          position: "bottom-right"
       })
    }
    navigate("/product")
  });

  useEffect(() =>{
    async function loadProduct(){
      if(params.id){
        const res = await getProduct(params.id);
        console.log(res)
        setValue('id', res.data.id)
        setValue('nombre', res.data.nombre)
        setValue('precio', res.data.precio)
      }
    }
    loadProduct();
  },[])

  return (
    <div className="max-w-xl mx-auto" >
      <form onSubmit={onSubmit}>
        <input 
          type="text"
          placeholder="Id"
          {...register("id", { required: true })}
          className="bg-zinc-500 p-3 rounded-lg block w-full mb-3"
          />
          {errors.id && <span>El campo es requerido</span>}
        <input 
          type="text"
          placeholder="Nombre"
          {...register("nombre", { required: true })}
          />
          {errors.nombre && <span>El campo es requerido</span>}
        <input 
          type="text" 
          placeholder="Precio"
          {...register("precio", { required: true })}
          />
          {errors.precio && <span>El campo es requerido</span>}
        <button className="bg-indigo-400 p-3 rounded-lg block w-full mt-3"> 
          Guardar</button>

      </form>
        {params.id && (<button
        className="bg-red-500 p-3 rounded-lg w-48 mt-3"
        onClick={
         async() => {
           const accepted = window.confirm('seguro que quiere borrar?');
           if(accepted){
              await deleteProduct(params.id);
              toast.success('Producto Eliminado',{
                position: "bottom-right"
             });
             navigate("/product")   
            }
          }}
          > 
          Borrar 
          </button>
          )}

      </div>  
  );
}
